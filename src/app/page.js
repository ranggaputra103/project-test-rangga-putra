'use client'

import CardValuesComponent from "@/components/cardValuesComponent"
import CarouselComponent from "@/components/carouselComponent"
import FooterComponent from "@/components/footerComponent"
import FormContactUsComponent from "@/components/formContactUsComponent"
import NavbarComponent from "@/components/navbarComponent"

export default function Home() {
  return (
    <div>
      <NavbarComponent />
      <CarouselComponent />
      <CardValuesComponent/>
      {/*  */}
      <FormContactUsComponent />
      <FooterComponent/>
    </div>
  )
}
