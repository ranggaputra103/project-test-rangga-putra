import {
    Card,
    Input,
    Checkbox,
    Button,
    Typography,
  } from "@material-tailwind/react";
   
  export default function FormContactUsComponent() {
    return (
    <div className="mt-20 mb-20 flex justify-center">
        <Card color="transparent" shadow={false}    >
            <Typography variant="h4" color="blue-gray" className="flex justify-center">
                Contact Us
            </Typography>
            <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
                <div className="mb-4 flex flex-col gap-6">
                <Input size="lg" label="Name" required/>
                <Input size="lg" label="Email" type="email" required/>
                <Input size="lg" label="Message" required/>
                </div>
                <Button className="mt-6" fullWidth>
                Submit
                </Button>
            </form>
        </Card>
    </div>
    );
  }