import { Typography } from "@material-tailwind/react";
 
export default function FooterComponent() {
  return (
    <footer className="w-full bg-black p-8 mt-4">
      <hr className="my-8 border-blue-gray-50" />
      <Typography color="blue-gray" className="text-center font-normal">
        Copyright &copy; 2016 PT Company
      </Typography>
      <div className="flex justify-center">
        <img src="./img/facebook-official.png" alt="" className="w-8 mx-1 mt-3"/>
        <img src="./img/twitter.png" alt="" className="w-8 mx-1 mt-3"/>
      </div>
    </footer>
  );
}