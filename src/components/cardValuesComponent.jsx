import {
    Card,
    CardBody,
    CardFooter,
    Typography,
    Button
  } from "@material-tailwind/react";
   
  export default function CardValuesComponent() {
    return (
        // <div>
        //     <Typography variant="h4" color="blue-gray" className="flex justify-center">
        //         OUR
        //     </Typography>
        // </div>
        <div className="mt-20">
            <Typography variant="h4" color="blue-gray" className="flex justify-center">
                OUR VALUES
            </Typography>            
            <div className="mt-5 flex justify-center"> 
                <Card className="w-1/5 mx-1.5 bg-red-600 text-white">
                    <CardBody>
                        <img
                            src="./img/lightbulb-o.png"
                            alt="icon 1"
                            className="mx-auto w-8"
                        />
                        <Typography variant="h5" className="mb-2 flex justify-center">
                            Innovative
                        </Typography>
                        <Typography className="text-center text-sm">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum quasi deleniti id repellendus officiis, possimus debitis modi excepturi error quod repellat magni similique, doloribus eos saepe a, pariatur illum hic!
                        </Typography>
                    </CardBody>
                </Card>
                <Card className="w-1/5 mx-1.5 bg-green-800 text-white">
                    <CardBody>
                        <img
                            src="./img/bank.png"
                            alt="icon 1"
                            className="mx-auto w-1/5"
                        />
                        <Typography variant="h5" className="mb-2 flex justify-center">
                            Loyalty
                        </Typography>
                        <Typography className="text-center text-sm">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, quod labore dicta odio magnam suscipit ea accusantium neque velit dignissimos facilis blanditiis enim quia ipsum aperiam consequatur esse omnis tenetur!
                        </Typography>
                    </CardBody>
                </Card>
                <Card className="w-1/5 mx-1.5 bg-blue-500 text-white">
                    <CardBody>
                        <img
                            src="./img/balance-scale.png"
                            alt="icon 1"
                            className="mx-auto w-1/5"
                        />
                        <Typography variant="h5" className="mb-2 flex justify-center">
                            Respect
                        </Typography>
                        <Typography className="text-center text-sm">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae dolore dicta nulla temporibus facilis nihil sequi ducimus sed? Vero excepturi aut possimus, alias nesciunt illo exercitationem doloremque itaque aliquid doloribus.
                        </Typography>
                    </CardBody>
                </Card>
            </div>
        </div>
    );
  }