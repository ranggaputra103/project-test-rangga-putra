import { Carousel } from "@material-tailwind/react";

export default function CarouselComponent() {
  return (
    <Carousel
      className="rounded-xl !rounded-none"
      navigation={({ setActiveIndex, activeIndex, length }) => (
        <div className="absolute bottom-4 left-2/4 z-50 flex -translate-x-2/4 gap-2">
          {new Array(length).fill("").map((_, i) => (
            <span
              key={i}
              className={`block h-1 cursor-pointer rounded-2xl transition-all content-[''] ${
                activeIndex === i ? "bg-white w-8" : "bg-white/50 w-4"
              }`}
              onClick={() => setActiveIndex(i)}
            />
          ))}
        </div>
      )}
    >
      <img
        src="./img/bg.jpg"
        alt="image 1"
        className="h-full w-full object-cover"
      />
      <img
        src="./img/about-bg.jpg"
        alt="image 2"
        className="h-full w-full object-cover"
      />
    </Carousel>
  );
}